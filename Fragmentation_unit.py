"""
@authors: Heydar Khadem and Hoda Nemat

Please cite the following article if you find this code useful for your work:
    
https://doi.org/10.1016/j.talanta.2022.123379
"""

import math
import numpy as np

def Fragmentation_unit (X, No_FOLD):    
    """this function cuts signals into a number of euqal fragments"""
    FIRST_PART_FOLD = (len(X[0, :])) - (math.floor(len(X[0, :])/No_FOLD)*No_FOLD)
    if FIRST_PART_FOLD == 0:
        X_SPLITS = np.hsplit(X, No_FOLD)
    else:    
        NIR_FIRST_PART_END = FIRST_PART_FOLD * (math.floor(len(X[0, :])/No_FOLD)) + FIRST_PART_FOLD
        X_SPLITS_FIRST_PART = np.hsplit(X [:, 0: NIR_FIRST_PART_END], FIRST_PART_FOLD)
        X_SPLITS_SECOND_PART = np.hsplit(X [:, NIR_FIRST_PART_END:], No_FOLD-FIRST_PART_FOLD)
        X_SPLITS = X_SPLITS_FIRST_PART + X_SPLITS_SECOND_PART  
    return X_SPLITS