"""
@authors: Heydar Khadem and Hoda Nemat

Please cite the following article if you find this code useful for your work:
    
https://doi.org/10.1016/j.talanta.2022.123379

"""

from Regression_unit import Regression_unit
import numpy as np

def Concatenation_unit (X_train_fragments, X_test_fragments, Y_train, Y_test):
    """this function concatenates generates features of signals and creates feature vectors corresponding to signals""" 
    d_predictions_fragments = {}
    d_rmse_fragments = {}
    for i in range(len(X_train_fragments)):
        d_predictions_fragments['y_test_pred_nir_split_{}'.format(i+1)], d_predictions_fragments['y_train_pred_nir_split_{}'.format(i+1)],\
            d_rmse_fragments['rmse_test_nir_split_{}'.format(i+1)], _, _, _, _ \
                = Regression_unit (X_train_fragments[i], Y_train, X_test_fragments[i], Y_test)
        if i == 0:
            X_train_feature_vector = d_predictions_fragments['y_train_pred_nir_split_{}'.format(i+1)]
            X_test_feature_vector = d_predictions_fragments['y_test_pred_nir_split_{}'.format(i+1)]
            
        else:
            X_train_feature_vector = np.concatenate((X_train_feature_vector, d_predictions_fragments['y_train_pred_nir_split_{}'.format(i+1)]), axis=1)
            X_test_feature_vector = np.concatenate((X_test_feature_vector, d_predictions_fragments['y_test_pred_nir_split_{}'.format(i+1)]), axis=1)


    return X_train_feature_vector, X_test_feature_vector

