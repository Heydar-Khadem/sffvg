"""
@authors: Heydar Khadem and Hoda Nemat

Please cite the following article if you find this code useful for your work:
    
https://doi.org/10.1016/j.talanta.2022.123379

"""
import numpy as np
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_percentage_error
from math import sqrt
from sklearn.cross_decomposition import PLSRegression
from sys import stdout
from sklearn.model_selection import cross_val_predict


def Regression_unit (X_train, Y_train, X_test, Y_test, FS = 'not_rfe'):
    

    data_size = len(X_train[:, 0])
    
    CV = 10
    cni = [] 
    rmse = []
    r2 = []
    
    componentmax = np.minimum(X_train.shape[1], 10)
    component = np.arange(1, componentmax)
    for i in component:
        pls = PLSRegression(n_components=i)
        y_cv_train = cross_val_predict(pls, X_train, Y_train, cv=CV)

        msecv = mean_squared_error(Y_train, y_cv_train)
        PRESS = msecv*data_size
        cni_i = PRESS/(data_size-i-1)
        cni.append(cni_i)
        
        rmsecv = np.sqrt(msecv)
        rmse.append(rmsecv)
        r2_p = r2_score(Y_train ,y_cv_train)
        r2.append (r2_p)       
    
    msemin = np.argmin(rmse)
    
    
    n_components=msemin+1    
    model = PLSRegression(n_components)
    model.fit(X_train, Y_train)
    
    
    
    y_cv_train = cross_val_predict(model, X_train, Y_train, cv=CV)
    rmse_train_cv = sqrt(mean_squared_error(Y_train, y_cv_train, multioutput='raw_values'))
    y_pred_test = model.predict(X_test)
    # yhat_train = model.predict(X_train)
    
    
    
    rmse_test = sqrt(mean_squared_error(Y_test, y_pred_test, multioutput='raw_values')) 
    mape_test = mean_absolute_percentage_error (Y_test, y_pred_test)*100
    r2_test = r2_score(Y_test ,y_pred_test)
    

    return   y_pred_test, y_cv_train, rmse_test, rmse_train_cv, mape_test, r2_test, model


###############################################################################

def Regression_unit_CV (X, y):
         
    DATA_SIZE = len(X)
    CV = 10
    cni = [] 
    rmse = []
    r2 = []
    
    componentmax = np.minimum(X.shape[1], 10)
    component = np.arange(1, componentmax+1)    
    for i in component:
        pls1 = PLSRegression(n_components=i)
        y_cv = cross_val_predict(pls1, X, y, cv=CV)
     
        mse_cv = mean_squared_error(y, y_cv)
        press = mse_cv*DATA_SIZE
        cni.append(press/(DATA_SIZE-i-1))
        
        rmse_cv = np.sqrt(mse_cv)
        rmse.append(rmse_cv)
        r2_p = r2_score(y, y_cv)
        r2.append (r2_p)

        comp = 100*i/componentmax
        # Trick to update status on the same line
        stdout.write("\r%d%% completed" % comp)
        stdout.flush()
    stdout.write("\n")
    


    n_components = np.argmin(cni)+1
    pls2 = PLSRegression(n_components)
    pls2.fit(X, y)
    y_pred_cal = pls2.predict(X)
    y_cv = cross_val_predict(pls2, X, y, cv=CV)
    rmse_cv_optimume = sqrt (mean_squared_error(y_cv, y, multioutput='raw_values'))
    rmse_pred_cal_optimume = sqrt (mean_squared_error(y_pred_cal, y, multioutput='raw_values'))
    
    return (y_cv, y_pred_cal, rmse_cv_optimume, rmse_pred_cal_optimume, n_components)

#####################################################################################    