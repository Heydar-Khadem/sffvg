"""
@authors: Heydar Khadem and Hoda Nemat

Please cite the following article if you find this code useful for your work:
    
https://doi.org/10.1016/j.talanta.2022.123379

"""
from scipy.signal import savgol_filter


def Preprocessing_unit (X, Preprocessing_type):
    """this function preprocesses signals"""
    if Preprocessing_type == 'No_preprocessing':  
        X = X
    if Preprocessing_type == 'SG_smoothing':
        X = savgol_filter(X, 5, polyorder = 3, deriv=0)
        
    return (X)