"""
@authors: Heydar Khadem and Hoda Nemat

Please cite the following article if you find this code useful for your work:
    
https://doi.org/10.1016/j.talanta.2022.123379

"""


import numpy as np
from pandas import read_csv
from numpy import array
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from Regression_unit import Regression_unit
from Preprocessing_unit import Preprocessing_unit
from Fragmentation_unit import Fragmentation_unit
from Concatenation_unit import Concatenation_unit
from SHAP_unit import SHAP_unit
from Iplsr_unit import Iplsr_unit




"""
Preparations:
    Load your data. X as 2-D array where each row is a signal, and Y as 1-D array where each ellement is the reference glucose value of the associated signal.
    We have named them: X_train_nir, X_train_mir, X_train_nir_mir, X_test_nir, X_test_mir, X_test_nir_mir, Y_train, Y_test.
    In the following, define the optimum number of fragments for NIR and MIR and NIR MIR signals. For optimisation in each case as discussed in the paper you can grid search and choose the one providing the best results on training data.
    
names cosidered for variables:
    X_train_nir_mir, X_test_nir_mir, X_train_nir, X_test_nir, X_train_mir, X_test_mir, Y_train, Y_test, opt_fragment_nir, opt_fragment_mir, opt_fragment_nir_mir
    
"""


# define the preproceesing method manually here
PREPROCESSING_LIST = ['No_preprocessing', 'SG_smoothing']
preprocessing_type = PREPROCESSING_LIST [0]

###############################################################################
###############################################################################
## NIR Modelling

# no feature selection
X_train = Preprocessing_unit (X_train_nir.copy(), preprocessing_type)
X_test = Preprocessing_unit (X_test_nir.copy(), preprocessing_type)
_, _, rmse_nir_nofusion_noselection, _, mape_nir_nofusion_noselection, r2_nir_nofusion_noselection, _ = Regression_unit (X_train.copy(), Y_train.copy(), X_test.copy(), Y_test.copy())


# SFFVG
X_train_fragments = Fragmentation_unit (Preprocessing_unit (X_train_nir.copy(), preprocessing_type), opt_fragment_nir)
X_test_fragments = Fragmentation_unit (Preprocessing_unit (X_test_nir.copy(), preprocessing_type), opt_fragment_nir)
X_train_feature_vector, X_test_feature_vector = Concatenation_unit (X_train_fragments.copy(), X_test_fragments.copy(), Y_train.copy(), Y_test.copy())
y_pred_test_nir_nofusion_stackselection, _, rmse_nir_nofusion_stackselection, _, mape_nir_nofusion_stackselection, r2_nir_nofusion_stackselection, model = Regression_unit (X_train_feature_vector.copy(), Y_train.copy(), X_test_feature_vector.copy(), Y_test.copy())
SHAP_unit (X_train_feature_vector, X_test_feature_vector, model, '(a)', [r'f$^{1}_{NIR}$', r'f$^{2}_{NIR}$', r'f$^{3}_{NIR}$', r'f$^{4}_{NIR}$'], 4, 'NIR_SHAP')


# iPLS
X_train_iplsr, X_test_iplsr = Iplsr_unit (opt_fragment_nir, X_train_fragments, X_test_fragments, Y_train)
y_pred_test_nir_nofusion_iplsr, _, rmse_nir_nofusion_iplsr, _, mape_nir_nofusion_iplsr, r2_nir_nofusion_iplsr, model = Regression_unit (X_train_iplsr.copy(), Y_train.copy(), X_test_iplsr.copy(), Y_test.copy())
##############################################################################
###############################################################################
## MIR Modelling

# no feature selection
X_train = Preprocessing_unit (X_train_mir.copy(), preprocessing_type)
X_test = Preprocessing_unit (X_test_mir.copy(), preprocessing_type)
_, _, rmse_mir_nofusion_noselection, _, mape_mir_nofusion_noselection, r2_mir_nofusion_noselection, _ = Regression_unit (X_train.copy(), Y_train.copy(), X_test.copy(), Y_test.copy())


# SFFVG
X_train_fragments = Fragmentation_unit (Preprocessing_unit (X_train_mir.copy(), preprocessing_type), opt_fragment_mir)
X_test_fragments = Fragmentation_unit (Preprocessing_unit (X_test_mir.copy(), preprocessing_type), opt_fragment_mir)
X_train_feature_vector, X_test_feature_vector = Concatenation_unit (X_train_fragments.copy(), X_test_fragments.copy(), Y_train.copy(), Y_test.copy())
y_pred_test_mir_nofusion_stackselection, _, rmse_mir_nofusion_stackselection, _, mape_mir_nofusion_stackselection, r2_mir_nofusion_stackselection, model = Regression_unit (X_train_feature_vector.copy(), Y_train.copy(), X_test_feature_vector.copy(), Y_test.copy())
SHAP_unit (X_train_feature_vector, X_test_feature_vector, model, '(b)', [r'f$^{1}_{MIR}$', r'f$^{2}_{MIR}$', r'f$^{3}_{MIR}$', r'f$^{4}_{MIR}$', r'f$^{5}_{MIR}$', r'f$^{6}_{MIR}$'], 6, 'MIR_SHAP')


# iPLS
X_train_iplsr, X_test_iplsr = Iplsr_unit (opt_fragment_mir, X_train_fragments, X_test_fragments, Y_train)
y_pred_test_mir_nofusion_iplsr, _, rmse_mir_nofusion_iplsr, _, mape_mir_nofusion_iplsr, r2_mir_nofusion_iplsr, model = Regression_unit (X_train_iplsr.copy(), Y_train.copy(), X_test_iplsr.copy(), Y_test.copy())
###############################################################################
###############################################################################
## rRaw Spectra Fusion Modelling

# no feature selection
X_train = Preprocessing_unit (X_train_nir_mir.copy(), preprocessing_type)
X_test = Preprocessing_unit (X_test_nir_mir.copy(), preprocessing_type)
_, _, rmse_nir_mir_rawfusion_noselection, _, mape_nir_mir_rawfusion_noselection, r2_nir_mir_rawfusion_noselection, _ = Regression_unit (X_train.copy(), Y_train.copy(), X_test.copy(), Y_test.copy())


# SFFVG
X_train_fragments = Fragmentation_unit(X_train.copy(), opt_fragment_nir_mir)
X_test_fragments = Fragmentation_unit(X_test.copy(), opt_fragment_nir_mir)
X_train_feature_vector, X_test_feature_vector = Concatenation_unit (X_train_fragments.copy(), X_test_fragments.copy(), Y_train.copy(), Y_test.copy())
y_pred_test_nir_mir_rawfusion_stackselection, _, rmse_nir_mir_rawfusion_stackselection, _, mape_nir_mir_rawfusion_stackselection, r2_nir_mir_rawfusion_stackselection, model = Regression_unit (X_train_feature_vector.copy(), Y_train.copy(), X_test_feature_vector.copy(), Y_test.copy())
SHAP_unit (X_train_feature_vector, X_test_feature_vector, model, '(c)', [r'f$^{1}_{NIR-MIR}$', r'f$^{2}_{NIR-MIR}$', r'f$^{3}_{NIR-MIR}$', r'f$^{4}_{NIR-MIR}$', r'f$^{5}_{NIR-MIR}$', r'f$^{6}_{NIR-MIR}$', r'f$^{7}_{NIR-MIR}$', r'f$^{8}_{NIR-MIR}$', r'f$^{9}_{NIR-MIR}$', r'f$^{10}_{NIR-MIR}$'], 10, 'NIR_MIR_raw_fusion_SHAP')

# iPLS
X_train_iplsr, X_test_iplsr = Iplsr_unit (opt_fragment_nir_mir, X_train_fragments, X_test_fragments, Y_train)
y_pred_test_nir_mir_rawfusion_iplsr, _, rmse_nir_mir_rawfusion_iplsr, _, mape_nir_mir_rawfusion_iplsr, r2_nir_mir_rawfusion_iplsr, model = Regression_unit (X_train_iplsr.copy(), Y_train.copy(), X_test_iplsr.copy(), Y_test.copy())
#################################################################################
###############################################################################
## Preprocessed Spectra Fusion Modelling

# no feature selection
X_train = np.concatenate((Preprocessing_unit (X_train_nir.copy(), preprocessing_type), Preprocessing_unit (X_train_mir.copy(), preprocessing_type)), axis = 1)
X_test = np.concatenate((Preprocessing_unit (X_test_nir.copy(), preprocessing_type), Preprocessing_unit (X_test_mir.copy(), preprocessing_type)), axis = 1)
_, _, rmse_nir_mir_preprocessedfusion_noselection, _, mape_nir_mir_preprocessedfusion_noselection, r2_nir_mir_preprocessedfusion_noselection, _ = Regression_unit (X_train.copy(), Y_train.copy(), X_test.copy(), Y_test.copy())

# SFFVG
X_train_fragments = Fragmentation_unit(X_train.copy(), opt_fragment_nir_mir)
X_test_fragments = Fragmentation_unit(X_test.copy(), opt_fragment_nir_mir)
X_train_feature_vector, X_test_feature_vector = Concatenation_unit (X_train_fragments.copy(), X_test_fragments.copy(), Y_train.copy(), Y_test.copy())
y_pred_test_nir_mir_preprocessedfusion_stackselection, _, rmse_nir_mir_preprocessedfusion_stackselection, _, mape_nir_mir_preprocessedfusion_stackselection, r2_nir_mir_preprocessedfusion_stackselection, model = Regression_unit (X_train_feature_vector.copy(), Y_train.copy(), X_test_feature_vector.copy(), Y_test.copy())
SHAP_unit (X_train_feature_vector, X_test_feature_vector, model, '(d)', [r'f$^{1}_{NIR-MIR}$', r'f$^{2}_{NIR-MIR}$', r'f$^{3}_{NIR-MIR}$', r'f$^{4}_{NIR-MIR}$', r'f$^{5}_{NIR-MIR}$', r'f$^{6}_{NIR-MIR}$', r'f$^{7}_{NIR-MIR}$', r'f$^{8}_{NIR-MIR}$', r'f$^{9}_{NIR-MIR}$', r'f$^{10}_{NIR-MIR}$'], 10, 'NIR_MIR_preprocessed_fusion_SHAP')

# iPLS
X_train_iplsr, X_test_iplsr = Iplsr_unit (opt_fragment_nir_mir, X_train_fragments, X_test_fragments, Y_train)
y_pred_test_nir_mir_preprocessedfusion_iplsr, _, rmse_nir_mir_preprocessedfusion_iplsr, _, mape_nir_mir_preprocessedfusion_iplsr, r2_nir_mir_preprocessedfusion_iplsr, model = Regression_unit (X_train_iplsr.copy(), Y_train.copy(), X_test_iplsr.copy(), Y_test.copy())
###############################################################################
###############################################################################
## Feature Fusion Modelling


# SFFVG
X_train_fragments_nir = Fragmentation_unit (Preprocessing_unit (X_train_nir.copy(), preprocessing_type), opt_fragment_nir)
X_test_fragments_nir = Fragmentation_unit (Preprocessing_unit (X_test_nir.copy(), preprocessing_type), opt_fragment_nir)
X_train_feature_vector_nir, X_test_feature_vector_nir = Concatenation_unit (X_train_fragments_nir.copy(), X_test_fragments_nir.copy(), Y_train.copy(), Y_test.copy())

X_train_fragments_mir = Fragmentation_unit (Preprocessing_unit (X_train_mir.copy(), preprocessing_type), opt_fragment_mir)
X_test_fragments_mir = Fragmentation_unit (Preprocessing_unit (X_test_mir.copy(), preprocessing_type), opt_fragment_mir)
X_train_feature_vector_mir, X_test_feature_vector_mir = Concatenation_unit (X_train_fragments_mir.copy(), X_test_fragments_mir.copy(), Y_train.copy(), Y_test.copy())

X_train_feature_vector = np.concatenate((X_train_feature_vector_nir.copy(), X_train_feature_vector_mir.copy()), axis = 1)
X_test_feature_vector = np.concatenate((X_test_feature_vector_nir.copy(), X_test_feature_vector_mir.copy()), axis = 1)
y_pred_test_nir_mir_featurefusion_stackselection, _, rmse_nir_mir_featurefusion_stackselection, _, mape_nir_mir_featurefusion_stackselection, r2_nir_mir_featurefusion_stackselection, model = Regression_unit (X_train_feature_vector.copy(), Y_train.copy(), X_test_feature_vector.copy(), Y_test.copy())
SHAP_unit (X_train_feature_vector, X_test_feature_vector, model, '(e)', [r'f$^{1}_{NIR}$', r'f$^{2}_{NIR}$', r'f$^{3}_{NIR}$', r'f$^{4}_{NIR}$', r'f$^{1}_{MIR}$', r'f$^{2}_{MIR}$', r'f$^{3}_{MIR}$', r'f$^{4}_{MIR}$', r'f$^{5}_{MIR}$', r'f$^{6}_{MIR}$'], 10, "NIR_MIR_feature_fusion_SHAP")

# iPLS
X_train_iplsr, X_test_iplsr = Iplsr_unit (opt_fragment_nir_mir, X_train_fragments_nir+X_train_fragments_mir, X_test_fragments_nir+X_test_fragments_mir, Y_train)
y_pred_test_nir_mir_featurefusion_iplsr, _, rmse_nir_mir_featurefusion_iplsr, _, mape_nir_mir_featurefusion_iplsr, r2_nir_mir_featurefusion_iplsr, model = Regression_unit (X_train_iplsr.copy(), Y_train.copy(), X_test_iplsr.copy(), Y_test.copy())
#################################################################################
###############################################################################
## Decision Fusion Modelling

# no feature selection
X_train_nir_part = Preprocessing_unit (X_train_nir.copy(), preprocessing_type)
X_test_nir_part = Preprocessing_unit (X_test_nir.copy(), preprocessing_type)
decision_test_nir, decision_train_nir, _, _, _, _, _ = Regression_unit (X_train_nir_part.copy(), Y_train.copy(), X_test_nir_part.copy(), Y_test.copy())

X_train_mir_part = Preprocessing_unit (X_train_mir.copy(), preprocessing_type)
X_test_mir_part = Preprocessing_unit (X_test_mir.copy(), preprocessing_type)
decision_test_mir, decision_train_mir, _, _, _, _ , _= Regression_unit (X_train_mir_part.copy(), Y_train.copy(), X_test_mir_part.copy(), Y_test.copy())


decision_train_nir_mir = np.concatenate((decision_train_nir.copy(), decision_train_mir.copy()), axis = 1)
decision_test_nir_mir = np.concatenate((decision_test_nir.copy(), decision_test_mir.copy()), axis = 1)


_, _, rmse_nir_mir_dicisionfusion_noselection, _, mape_nir_mir_dicisionfusion_noselection, r2_nir_mir_dicisionfusion_noselection, _ = Regression_unit (decision_train_nir_mir.copy(), Y_train.copy(), decision_test_nir_mir.copy(), Y_test.copy())




# SFFVG
X_train_fragments_nir = Fragmentation_unit (Preprocessing_unit (X_train_nir.copy(), preprocessing_type), opt_fragment_nir)
X_test_fragments_nir = Fragmentation_unit (Preprocessing_unit (X_test_nir.copy(), preprocessing_type), opt_fragment_nir)


X_train_fragments_mir = Fragmentation_unit (Preprocessing_unit (X_train_mir.copy(), preprocessing_type), opt_fragment_mir)
X_test_fragments_mir = Fragmentation_unit (Preprocessing_unit (X_test_mir.copy(), preprocessing_type), opt_fragment_mir)

X_train_feature_vector_nir, X_test_feature_vector_nir = Concatenation_unit (X_train_fragments_nir.copy(), X_test_fragments_nir.copy(), Y_train.copy(), Y_test.copy())
X_train_feature_vector_mir, X_test_feature_vector_mir = Concatenation_unit (X_train_fragments_mir.copy(), X_test_fragments_mir.copy(), Y_train.copy(), Y_test.copy())


decision_test_nir, decision_train_nir, _, _, _, _, _= Regression_unit (X_train_feature_vector_nir.copy(), Y_train.copy(), X_test_feature_vector_nir.copy(), Y_test.copy())
decision_test_mir, decision_train_mir, _, _, _, _, _= Regression_unit (X_train_feature_vector_mir.copy(), Y_train.copy(), X_test_feature_vector_mir.copy(), Y_test.copy())

decision_train_nir_mir = np.concatenate((decision_train_nir.copy(), decision_train_mir.copy()), axis = 1)
decision_test_nir_mir = np.concatenate((decision_test_nir.copy(), decision_test_mir.copy()), axis = 1)


y_pred_test_nir_mir_dicisionfusion_stackselection, _, rmse_nir_mir_dicisionfusion_stackselection, _, mape_nir_mir_dicisionfusion_stackselection, r2_nir_mir_dicisionfusion_stackselection, model = Regression_unit (decision_train_nir_mir.copy(), Y_train.copy(), decision_test_nir_mir.copy(), Y_test.copy())
SHAP_unit (decision_train_nir_mir, decision_test_nir_mir, model, '(f)', [r'd$^{}_{NIR}$', r'd$^{}_{MIR}$'], 2, "NIR_MIR_decision_fusion_SHAP")


# iPLS
X_train_nir_iplsr, X_test_nir_iplsr = Iplsr_unit (opt_fragment_nir, X_train_fragments_nir, X_test_fragments_nir, Y_train)
decision_test_nir, decision_train_nir, _, _, _, _, _ = Regression_unit (X_train_nir_iplsr.copy(), Y_train.copy(), X_test_nir_iplsr.copy(), Y_test.copy())

X_train_mir_iplsr, X_test_mir_iplsr = Iplsr_unit (opt_fragment_mir, X_train_fragments_mir, X_test_fragments_mir, Y_train)
decision_test_mir, decision_train_mir, _, _, _, _, _ = Regression_unit (X_train_mir_iplsr.copy(), Y_train.copy(), X_test_mir_iplsr.copy(), Y_test.copy())


decision_train_nir_mir = np.concatenate((decision_train_nir.copy(), decision_train_mir.copy()), axis = 1)
decision_test_nir_mir = np.concatenate((decision_test_nir.copy(), decision_test_mir.copy()), axis = 1)


y_pred_test_nir_mir_dicisionfusion_iplsr, _, rmse_nir_mir_dicisionfusion_iplsr, _, mape_nir_mir_dicisionfusion_iplsr, r2_nir_mir_dicisionfusion_iplsr, model = Regression_unit (decision_train_nir_mir.copy(), Y_train.copy(), decision_test_nir_mir.copy(), Y_test.copy())
###############################################################################
###############################################################################





























