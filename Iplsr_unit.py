"""
@authors: Heydar Khadem and Hoda Nemat

Please cite the following article if you find this code useful for your research:
    
https://doi.org/10.1016/j.talanta.2022.123379

"""

import numpy as np
from Regression_unit import Regression_unit_CV


def Iplsr_unit (n_fold, X_train_fragments, X_test_fragments, Y_train):
    """this function selects some intervals (fragments)"""
    intervals_order = [0]*n_fold
    rmse_order = [0]*n_fold
    indices = list(range(len(X_train_fragments)))
    
    for l in range(n_fold):
        
        rmse_cv_all = [0]*(n_fold-l)
        j = 0
        if l ==0:
            for i in indices:
                _, _, rmse_cv, _, _ = Regression_unit_CV(X_train_fragments[i], Y_train)
                rmse_cv_all [j] = rmse_cv
                j += 1
            selected_in = indices[rmse_cv_all.index(min(rmse_cv_all))]
            intervals_order [l] = selected_in 
            rmse_order [l] = min(rmse_cv_all)
            X_train_iplsr = X_train_fragments [selected_in]
            X_test_iplsr = X_test_fragments [selected_in]
            indices.remove(selected_in)
        
            
        elif l !=0:
            for i in indices:
                X_train_tem = np.concatenate((X_train_iplsr, X_train_fragments[i]), axis =1)
                _, _, rmse_cv, _, _ = Regression_unit_CV(X_train_tem, Y_train)
                rmse_cv_all [j] = rmse_cv
                j += 1
        
            selected_in = indices[rmse_cv_all.index(min(rmse_cv_all))]
            if min(rmse_cv_all) < rmse_order [l-1]:
                intervals_order [l] = selected_in 
                rmse_order [l] = min(rmse_cv_all)
                X_train_iplsr = np.concatenate((X_train_iplsr, X_train_fragments [selected_in]), axis =1)
                X_test_iplsr = np.concatenate((X_test_iplsr, X_test_fragments [selected_in]), axis =1)
                indices.remove(selected_in)
            else:
                break
    return (X_train_iplsr, X_test_iplsr)
