"""
@authors: Heydar Khadem and Hoda Nemat

Please cite the following article if you find this code useful for your work:
    
https://doi.org/10.1016/j.talanta.2022.123379

"""

import pandas as pd
from matplotlib import pyplot as plt
import shap
from PIL import Image

def SHAP_unit (X_train, X_test, model, sup, columns_name, max_dis, fig_name):
    
    X_train = pd.DataFrame (X_train)
    
    X_test = pd.DataFrame (X_test)
    X_test.columns = columns_name
    
    explainer = shap.Explainer(model.predict, X_train)
    shap_values = explainer(X_test)
    
    plt.figure(figsize=(10, 8))

    shap.plots.bar(shap_values, max_display= max_dis, merge_cohorts=True, show = False)

    plt.rcParams['figure.figsize'] = (10, 8)
    plt.ylabel("features", fontsize = 18)
    plt.xlabel("mean(|SHAP value|)", fontsize = 18)
    plt.tight_layout()
    plt.suptitle(sup, fontsize = 22)
    plt.xticks(fontsize = 16)
    plt.yticks(fontsize = 16)
    
    COLOR = 'blue'
    plt.rcParams['text.color'] = COLOR
    plt.rcParams['axes.labelcolor'] = COLOR
    plt.rcParams['xtick.color'] = COLOR
    plt.rcParams['ytick.color'] = COLOR
    plt.show()
    plt.savefig(fname=fig_name+'.tiff', dpi=500, facecolor='w', edgecolor='w',
            orientation='portrait', format='tiff', pad_inches=0.1)
    Image.open(fig_name+'.tiff').convert('L').save(fig_name+'_gray.tiff')
    return()